package com.example.project2.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.project2.OnClickListener
import com.example.project2.R
import com.example.project2.databinding.ItemCharacterBinding
import com.example.project2.model.RMCharacter

class CharacterAdapter(private var characters: List<RMCharacter>, private val listener: OnClickListener):
    RecyclerView.Adapter<CharacterAdapter.ViewHolder>() {

    private lateinit var context: Context

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
                val binding = ItemCharacterBinding.bind(view)
        fun setListener(character: RMCharacter) {
            binding.root.setOnClickListener {
                listener.onClick(character)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_character, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val character = characters[position]
        with(holder){
            setListener(character)
            binding.textViewName.text = character.name
            binding.textViewOrder.text = character.id.toString()
            Glide.with(context)
                .load(character.image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(binding.imageViewImage)
        }
    }

    override fun getItemCount(): Int {
        return characters.size
    }

    fun setList(newCharacters: List<RMCharacter>) {
        characters = newCharacters
        notifyDataSetChanged()
    }

}