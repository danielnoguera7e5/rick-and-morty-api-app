package com.example.project2.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.project2.model.RMCharacter

@Dao
interface CharacterDao {
    @Query("SELECT * FROM CharacterEntity")
    fun getAllCharacters(): MutableList<RMCharacter>

    @Query("SELECT * FROM CharacterEntity where name = :characterName")
    fun getCharactersByName(characterName: String): MutableList<RMCharacter>

    @Query("UPDATE CharacterEntity set favourite = 'true' where id = :CharacterId")
    fun addFavourite(CharacterId: Int?): MutableList<RMCharacter>

    @Query("UPDATE CharacterEntity set favourite = 'false' where id = :CharacterId")
    fun deleteFavourite(CharacterId: Int?): MutableList<RMCharacter>

    @Query("SELECT * FROM CharacterEntity where favourite = 'true'")
    fun getAllFavourites(): MutableList<RMCharacter>

    @Insert
    fun addCharacter(characterEntity: CharacterEntity)
}
