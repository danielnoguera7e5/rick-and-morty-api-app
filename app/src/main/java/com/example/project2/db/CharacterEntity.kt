package com.example.project2.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.project2.model.Location
import com.example.project2.model.Origin


@Entity(tableName = "CharacterEntity")
data class CharacterEntity(
    @PrimaryKey
    var id: Int?,
    val created: String?,
    val episode: List<String?>?,
    val gender: String?,
    val image: String?,
    val location: Location?,
    val name: String?,
    val origin: Origin?,
    val species: String? ,
    val status: String?,
    val type: String?,
    val url: String?,
    var favourite: Boolean)


