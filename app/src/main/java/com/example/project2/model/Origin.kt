package com.example.project2.model

data class Origin(
    val name: String? = null,
    val url: String? = null
)