package com.example.project2.model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.project2.R
import com.example.project2.Repository
import com.example.project2.db.CharacterApplication
import com.example.project2.db.CharacterEntity
import java.util.logging.Handler

class CharacterViewModel: ViewModel() {

    val repository = Repository()
    var selectedCharacter = MutableLiveData<RMCharacter>()
    var data = MutableLiveData<Data>().apply { value = Data(Info(), listOf<RMCharacter>()) }

    init {
        fetchData("character")
        while (data.value != null) {
            data.value?.info?.next?.let { fetchData(it) }

        }
    }

    // MODIFICAR REPOSITORY + RECORRER CADA CHARACTER/ID Y METERLO EN LISTA
    // CACHE: CARGAR API Y SINO CARGAR DB

    fun fetchData(url: String){
        repository.fetchData(url)
        android.os.Handler().postDelayed({
            data.postValue(repository.dataInfo.value!!) },
            1000)
    }

    fun select(character: RMCharacter) {
        selectedCharacter.postValue(character)
        addFavourite(character.id)
    }

    fun addFavourite(id: Int?){
        CharacterApplication.database.characterDao().addFavourite(id)
    }

    fun characterToEntity(character: RMCharacter): CharacterEntity {


    }


}