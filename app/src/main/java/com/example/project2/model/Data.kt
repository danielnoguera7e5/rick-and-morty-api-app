package com.example.project2.model

data class Data(
    val info: Info,
    val results: List<RMCharacter>
)