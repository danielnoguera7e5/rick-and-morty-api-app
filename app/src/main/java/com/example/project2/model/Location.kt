package com.example.project2.model

data class Location(
    val name: String? = null,
    val url: String? = null
)