package com.example.project2.model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.project2.FavouriteRepository

class FavouriteViewModel: ViewModel() {

    var roomRepository = FavouriteRepository()
    var selectedCharacter = MutableLiveData<RMCharacter>()
    var data = MutableLiveData<List<RMCharacter>>().apply { value = listOf<RMCharacter>() }


    fun select(character: RMCharacter) {
        selectedCharacter.postValue(character)
    }

}