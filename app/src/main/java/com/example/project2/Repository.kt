package com.example.project2

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.project2.model.Data
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Repository {

    private val apiInterface = ApiInterface.create()
    var dataInfo = MutableLiveData<Data>()

    fun fetchData(url: String) {
        val call = apiInterface.getData(url)
        call.enqueue(object: Callback<Data> {
            override fun onFailure(call: Call<Data>, t: Throwable) {
                Log.e("ERROR", t.message.toString())
            }
            override fun onResponse(call: Call<Data?>, response: Response<Data?>) {
                if (response != null && response.isSuccessful) {
                    dataInfo.value = response.body()
                }
            }
        })
//        return dataInfo
    }
}