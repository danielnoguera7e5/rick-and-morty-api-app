package com.example.project2.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.project2.databinding.FragmentDetailBinding
import com.example.project2.model.CharacterViewModel
import com.example.project2.model.RMCharacter


class DetailFragment : Fragment() {
    private val model: CharacterViewModel by activityViewModels()
    lateinit var binding: FragmentDetailBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentDetailBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        model.selectedCharacter.observe(viewLifecycleOwner, Observer {
            val character = it
            Glide.with(requireContext())
                .load((character as RMCharacter).image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(binding.imageView)
            binding.textViewName.text = character.name
            binding.characterId.text = character.id.toString()
            binding.characterStatus.text = character.status
            binding.characterSpecies.text = character.species
            binding.characterGender.text = character.gender
            binding.characterLocation.text = character.location?.name
            binding.characterOrigin.text = character.origin?.name
        })
    }
}