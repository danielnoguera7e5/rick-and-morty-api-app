package com.example.project2.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.project2.OnClickListener
import com.example.project2.R
import com.example.project2.adapter.CharacterAdapter
import com.example.project2.databinding.FragmentRecyclerViewBinding
import com.example.project2.model.CharacterViewModel
import com.example.project2.model.Data
import com.example.project2.model.RMCharacter

class RecyclerViewFavouritesFragment : Fragment(), OnClickListener {

    //////////////////
    private lateinit var characterAdapter: CharacterAdapter
    //////////////////
    private lateinit var gridLayoutManager: RecyclerView.LayoutManager
    private lateinit var binding: FragmentRecyclerViewBinding
    private val model: CharacterViewModel by activityViewModels()

    // SET AND CREATE DATABASE FAVOURITE LIST

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentRecyclerViewBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setUpRecyclerView(model.data.value)
        model.data.observe(viewLifecycleOwner, Observer {
            // SETLIST ESTABLISHES FINAL ADAPTER LIST
            characterAdapter.setList(it.results)
            /////////////////////
            if (model.data.value == null) {
                Toast.makeText(context, "ERROR", Toast.LENGTH_SHORT).show()
            } else {
                setUpRecyclerView(it)
                binding.previousPageButton.isEnabled = model.data.value!!.info.prev != null
                binding.nextPageButton.isEnabled = model.data.value!!.info.next != null
            }
        })

        binding.previousPageButton.isEnabled = false
        binding.nextPageButton.isEnabled = false
        binding.previousPageButton.setOnClickListener {
            model.data.value?.let {
                model.fetchData(it.info.prev!!)
            }
        }
        binding.nextPageButton.setOnClickListener {
            model.data.value?.let {
                model.fetchData(it.info.next!!)
            }
        }
    }

    private fun setUpRecyclerView(data: Data?) {
        gridLayoutManager = GridLayoutManager(context, 2)
        if (data != null) {
            ////////////////////
            characterAdapter = CharacterAdapter(data.results, this)
            ////////////////////
            binding.recyclerView.apply {
                setHasFixedSize(true)
                layoutManager = gridLayoutManager
                adapter = characterAdapter
            }
        }
    }

    override fun onClick(character: RMCharacter) {
        model.select(character)
        parentFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, DetailFragment())
            setReorderingAllowed(true)
            addToBackStack(null)
            commit()
        }
    }
}