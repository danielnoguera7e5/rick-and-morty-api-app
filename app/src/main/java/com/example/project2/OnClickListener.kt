package com.example.project2

import com.example.project2.model.RMCharacter

interface OnClickListener {
    fun onClick(character: RMCharacter)
}